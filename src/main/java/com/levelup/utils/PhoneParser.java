package com.levelup.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PhoneParser {

    public void parse(URL url) throws IOException {
        System.out.println(url);
        URLConnection urlConnection = url.openConnection();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String line;
        List<String> strings = new ArrayList<>();
        while ((line = bufferedReader.readLine()) != null) {
            if (line.equals("0")) {
                break;
            }
            strings.add(line);
        }

        //strings.forEach(string -> {}System.out.println(HtmlParser.parseForPhone(string)));

        List<URL> urls = strings.stream()
                .flatMap(string -> HtmlParser.parseForUrl(string).stream())
                .collect(Collectors.toList());

        urls.parallelStream().forEach(url1 -> {
            try {
                Thread.sleep(1000);
                parse(url1);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

}
