import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public class URLTest {


    @Test
    public void urlTest() {

        try {
            URL url = new URL("http://www.korrespondent.net");
            try {
                URLConnection urlConnection = url.openConnection();
                System.out.println(urlConnection.getContentType());
                System.out.println(urlConnection.getContentEncoding());
                Map<String, List<String>> headerFields = urlConnection.getHeaderFields();

                headerFields.entrySet().forEach(entry -> {
                    System.out.print(entry.getKey()+ " ");
                    System.out.println(entry.getValue());
                });

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                    if(line.equals("0")) {
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }


}
