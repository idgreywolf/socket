package com.levelup.socket.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

import static com.levelup.utils.SockeUtils.sendMessage;

public class SocketClient {


    public void connect() {
        try (Socket socket = new Socket("10.47.91.51", 8080)) {

            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
            Random random = new Random();

            int clientId = random.nextInt();

            while (true) {
                sendMessage("Client " + clientId + " : Hi Server!", printWriter);
                Thread.sleep(3000);
            }


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
