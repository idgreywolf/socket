import com.levelup.utils.HtmlParser;
import org.junit.Assert;
import org.junit.Test;

import java.net.URL;
import java.util.List;

public class HtmlParseTest {


    @Test
    public void testUrlParser() {
        String text = "dfjbhxcjknhckjbhxcvbhjvn http://www.google.com jbhxcvhbjxcbhjxbhjbvhjhjbv www.korrespondent.net hjbhjhbjbjdfjjkjkdffjk" +
                "http://gorod.dp.ua dfgdfgdfg";
        List<URL> urls = HtmlParser.parseForUrl(text);
        Assert.assertEquals(2, urls.size());
    }

    @Test
    public void testPhoneParser() {
        String text = "dfgdfgdgf (056) 343-3434";
        List<String> phones = HtmlParser.parseForPhone(text);
        Assert.assertEquals(1, phones.size());
    }
}
