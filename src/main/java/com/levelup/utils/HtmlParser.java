package com.levelup.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlParser {


    public static List<URL> parseForUrl(String text) {
        List<URL> urls = new ArrayList();

        String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        while (m.find()) {
            String urlStr = m.group();
            if (urlStr.startsWith("(") &&
                    urlStr.endsWith(")")) {
                urlStr = urlStr.substring(1, urlStr.length() - 1);
            }
            try {
                if (!urlStr.startsWith("http://")) {
                    urlStr = "http://" + urlStr;
                }
                urls.add(new URL(urlStr));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public static List<String> parseForPhone(String text) {
        List<String> phones = new ArrayList<>();
        Pattern p = Pattern.compile("\\(\\d{3}\\)\\s\\d{3}-\\d{4}");

        Matcher m = null;
        String line = text;
        if (line != null && (m = p.matcher(line)).find()) {
            phones.add(line);
        }

        return phones;
    }
}
