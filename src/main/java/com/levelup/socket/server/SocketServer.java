package com.levelup.socket.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;


public class SocketServer {


    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(8080)) {

            while (true) {
                System.out.println("Server waiting!");
                Socket socket = serverSocket.accept();
                new Thread(() -> {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            System.out.println("Message received: " + line);
                        }
                    }
                } catch(IOException e){
                    e.printStackTrace();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }).start();
            System.out.println("Request received!");
        }

    } catch(
    IOException e)

    {
        e.printStackTrace();
    }
}
}
