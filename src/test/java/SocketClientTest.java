import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClientTest {

    @Test
    public void testSocketClient() {
        try (Socket socket = new Socket("www.korrespondent.net", 80)) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
            printWriter.println("GET / HTTP/1.1");
            printWriter.println("Host: korrespondent.net");
            printWriter.println("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
            printWriter.println("");

            printWriter.flush();


            String line;

            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                if (line.equals("0")) {
                    break;
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
